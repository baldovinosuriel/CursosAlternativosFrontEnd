package com.cursos.alternativos.portal.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.cursos.alternativos.portal.dto.CalificacionesAlumnoDTO;
import com.cursos.alternativos.portal.response.CalificacionesAlumnoResponse;
import com.cursos.alternativos.portal.response.Response;
import com.cursos.alternativos.portal.util.ClienteREST;
import com.google.gson.Gson;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class CalificacionesController extends BaseController{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Listbox lbxCalificaciones;
	private Label lblPromedio;
	private Label lblNombreAlumno;
	private Window modalDialogDelete;
	private Window modalDialogActualiza;
	private Label msgDelete;
	private CalificacionesAlumnoResponse calificacionesAlumnoResponse;
	private Textbox txtCalificacion;
	private Label errorCalificacion;
	private Label msgActualiza;
	private Button btnPdf;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception{
		super.doAfterCompose(comp);
		
		modalDialogDelete.setVisible(false);
		
		msgDelete.setValue("");
		msgActualiza.setValue("");
		obtieneRegistros();
			
	}
	
	public void onEliminaRegistro(ForwardEvent event) {
		 int idCalificacion = (int)event.getData();
		 session.setAttribute("idCalificacion", String.valueOf(idCalificacion));
		 modalDialogDelete.doModal();
		 modalDialogDelete.setVisible(true);
	}
	
	public void onActualizaRegistro(ForwardEvent event) {
		 int idCalificacion = (int)event.getData();
		 session.setAttribute("idCalificacion", String.valueOf(idCalificacion));
		 modalDialogActualiza.doModal();
		 modalDialogActualiza.setVisible(true);
	}
	
	public void onRegresar(ForwardEvent event) {
		Executions.getCurrent().sendRedirect("alumnos.zul");
	}
	
	public void onAgregarCalificacion(ForwardEvent event) {
		Executions.getCurrent().sendRedirect("addCalificacion.zul");
	}
	
	public void onExportaPDF(ForwardEvent event) throws JRException, IOException {
		String filePath = "C:\\reportes\\plantillaReporte.jrxml";
		
		JRBeanCollectionDataSource datasource = new JRBeanCollectionDataSource(calificacionesAlumnoResponse.getListaCalificaciones());
		Map<String, Object> parameters = new HashMap<String, Object>();
		final String format = ".00";
		DecimalFormat formatter = new DecimalFormat(format);
		BigDecimal promedio = new BigDecimal(formatter.format(calificacionesAlumnoResponse.getPromedio()));
		parameters.put("nombreAlumno",calificacionesAlumnoResponse.getListaCalificaciones().get(0).getNombre()+" "+calificacionesAlumnoResponse.getListaCalificaciones().get(0).getAp_paterno()+" "+calificacionesAlumnoResponse.getListaCalificaciones().get(0).getAp_materno());
		parameters.put("promedio", promedio);
		JasperReport report = JasperCompileManager.compileReport(filePath);
		
		JasperPrint print = JasperFillManager.fillReport(report, parameters,datasource);
		
		JasperExportManager.exportReportToPdfFile(print,"C:\\reportes\\ReporteCalificaciones.pdf");
				
	}
	
	public void onDeleteBtn(ForwardEvent event) {
		String idCalificacion = (String) session.getAttribute("idCalificacion");
		Response response = servicioEliminaCalificacion(Integer.valueOf(idCalificacion));
		
		if(response.getSucces().equals("ok")) {
			msgDelete.setStyle("color: green");
			obtieneRegistros();
		}else {
			msgDelete.setStyle("color: red");
		}
		msgDelete.setValue(response.getMsg());
		modalDialogDelete.setVisible(false);
    }
	
	public void onCloseModalDeleteBtn(ForwardEvent event) {
		modalDialogDelete.setVisible(false);
    }
	
	public void onActualizaBtn(ForwardEvent event) {
		
		BigDecimal calificacion = null;
		errorCalificacion.setValue("");
		
		if(txtCalificacion.getValue() == null || txtCalificacion.getValue().equals("")) {
			errorCalificacion.setValue("Debe ingresar una calificación");
		}else {
			calificacion = new BigDecimal(txtCalificacion.getValue());
			System.out.println("Calificacion: " + calificacion);
			
			int resultadoMayor = calificacion.compareTo(new BigDecimal("10"));
			int resultadoMenor = calificacion.compareTo(new BigDecimal("0"));
			
			if(resultadoMayor == 1) {
				errorCalificacion.setValue("Debe ingresar una calificación entre 0 y 10");
			}
			
			if(resultadoMenor == -1) {
				errorCalificacion.setValue("Debe ingresar una calificación entre 0 y 10");
			}
		}
		
		if(errorCalificacion.getValue().equals("")) {
			String idCalificacion = (String) session.getAttribute("idCalificacion");
			Response response = servicioActualizaCalificacion(calificacion,Integer.valueOf(idCalificacion));
			
			if(response.getSucces().equals("ok")) {
				msgActualiza.setStyle("color: green");
				obtieneRegistros();
			}else {
				msgActualiza.setStyle("color: red");
			}
			msgActualiza.setValue(response.getMsg());
			modalDialogActualiza.setVisible(false);
			
		}else {
			return;
		}
		
    }
	
	public void onCloseModalActualizaBtn(ForwardEvent event) {
		modalDialogActualiza.setVisible(false);
    }
	
	private void obtieneRegistros() {
		
		String idAlumno = (String) session.getAttribute("idAlumno");
		
		System.out.println("Id alumno en sesión: "+idAlumno);
		CalificacionesAlumnoResponse response = servicioConsultaCalif(idAlumno);
		calificacionesAlumnoResponse = response;
		lbxCalificaciones.getItems().clear();
		lbxCalificaciones.setModel(new ListModelList<CalificacionesAlumnoDTO>(response.getListaCalificaciones()));
			
		if(!response.getListaCalificaciones().isEmpty()) {
			btnPdf.setVisible(true);
			final String format = ".00";
			DecimalFormat formatter = new DecimalFormat(format);
			BigDecimal promedio = new BigDecimal(formatter.format(response.getPromedio()));
			lblPromedio.setValue("Promedio : "+String.valueOf(promedio));
			lblNombreAlumno.setValue(response.getListaCalificaciones().get(0).getNombre()+" "+response.getListaCalificaciones().get(0).getAp_paterno()+" "+response.getListaCalificaciones().get(0).getAp_materno());
		}else {
			btnPdf.setVisible(false);
			lblPromedio.setValue("");
			String nombreAlumno = (String) session.getAttribute("nombreAlumno");
			lblNombreAlumno.setValue(nombreAlumno);
		}
    }
		
	private CalificacionesAlumnoResponse servicioConsultaCalif(String idAlumno){
		
		String urlServicio = "http://localhost:8080/CursosAlternativosBackEnd/rest/consultaCalif?idAlumno="+idAlumno;
		String jsonResponse = ClienteREST.get(urlServicio, "application/json", "application/json");
		System.out.println("jsonResponse: " + jsonResponse);
		CalificacionesAlumnoResponse response = (CalificacionesAlumnoResponse) new Gson().fromJson(jsonResponse, CalificacionesAlumnoResponse.class);
		System.out.println("Response: " + response);
		
		return response;
		
	}
	
	private Response servicioEliminaCalificacion(int idCalificacion){
		
		String urlServicio = "http://localhost:8080/CursosAlternativosBackEnd/rest/eliminaCalificacion/"+idCalificacion;
		String jsonResponse = ClienteREST.delete(urlServicio, "application/json", "application/json");
		System.out.println("jsonResponse: " + jsonResponse);
		Response response = (Response) new Gson().fromJson(jsonResponse, Response.class);
		System.out.println("Response: " + response);
		
		return response;
		
	}
	
	private Response servicioActualizaCalificacion(BigDecimal calificacion,int idCalificacion){
		
		String urlServicio = "http://localhost:8080/CursosAlternativosBackEnd/rest/actualizaCalificacion/"+idCalificacion+"/"+calificacion;
		String jsonRequest = new Gson().toJson("");
		String jsonResponse = ClienteREST.put(urlServicio, "application/json", "application/json",jsonRequest);
		System.out.println("jsonResponse: " + jsonResponse);
		Response response = (Response) new Gson().fromJson(jsonResponse, Response.class);
		System.out.println("Response: " + response);
		
		return response;
		
	}
	
}

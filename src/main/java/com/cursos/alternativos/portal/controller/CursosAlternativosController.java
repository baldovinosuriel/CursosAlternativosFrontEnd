package com.cursos.alternativos.portal.controller;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;

import com.cursos.alternativos.portal.dto.AlumnosDTO;
import com.cursos.alternativos.portal.util.ClienteREST;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CursosAlternativosController extends BaseController{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Listbox lbxAlumnos;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception{
		super.doAfterCompose(comp);
		List<AlumnosDTO> listaAlumnos = servicioConsultaAlumnos();
		
		lbxAlumnos.getItems().clear();
		lbxAlumnos.setModel(new ListModelList<AlumnosDTO>(listaAlumnos));
	
	}
	
	public void onShowAlert(ForwardEvent event) {
		AlumnosDTO alumno = (AlumnosDTO)event.getData();
		System.out.println("Id alumno: "+String.valueOf(alumno.getId_t_usuarios()));
		session.setAttribute("nombreAlumno", alumno.getNombre()+" "+alumno.getAp_paterno()+" "+alumno.getAp_materno());
		session.setAttribute("idAlumno", String.valueOf(alumno.getId_t_usuarios()));
		Executions.getCurrent().sendRedirect("calificaciones.zul");
	}
	
	private List<AlumnosDTO> servicioConsultaAlumnos(){
		
		String urlServicio = "http://localhost:8080/CursosAlternativosBackEnd/rest/consultaAlumnos";
		String jsonResponse = ClienteREST.get(urlServicio, "application/json", "application/json");
		System.out.println("jsonResponse: " + jsonResponse);
		List<AlumnosDTO> listaAlumnos = null;
		try {
		
			ObjectMapper objectMapper = new ObjectMapper();
			listaAlumnos = objectMapper.readValue(jsonResponse, objectMapper.getTypeFactory().constructCollectionType(List.class, AlumnosDTO.class));
			System.out.println("listaAlumnos: " + listaAlumnos.toString());
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return listaAlumnos;
		
	}
	
}

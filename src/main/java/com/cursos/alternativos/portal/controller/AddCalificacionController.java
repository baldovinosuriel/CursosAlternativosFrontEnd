package com.cursos.alternativos.portal.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.cursos.alternativos.portal.dto.CalificacionesDTO;
import com.cursos.alternativos.portal.dto.MateriasDTO;
import com.cursos.alternativos.portal.response.Response;
import com.cursos.alternativos.portal.util.ClienteREST;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class AddCalificacionController extends BaseController{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Listbox lstboxMateria;
	private Textbox txtCalificacion;
	private Label errorMateria;
	private Label errorCalificacion;
	private Label msg;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception{
		super.doAfterCompose(comp);
		
		List<MateriasDTO> listaMaterias = servicioConsultaMaterias();
		lstboxMateria.getItems().clear();
		lstboxMateria.setModel(new ListModelList<MateriasDTO>(listaMaterias));
		
	}
	
	public void onAgregarCalificacion(ForwardEvent event) throws ParseException {
		CalificacionesDTO calificacionDTO = new CalificacionesDTO();
		int idMateria = 0;
		BigDecimal calificacion = null;
		errorMateria.setValue("");
		errorCalificacion.setValue("");
		try {
			
			if(lstboxMateria.getSelectedItem() == null ) {
				errorMateria.setValue("Debe seleccionar una materia");
			}else {
				idMateria = lstboxMateria.getSelectedItem().getValue();
				System.out.println("idMateria: " + idMateria);
			}
			
			if(txtCalificacion.getValue() == null || txtCalificacion.getValue().equals("")) {
				errorCalificacion.setValue("Debe ingresar una calificación");
			}else {
				calificacion = new BigDecimal(txtCalificacion.getValue());
				System.out.println("Calificacion: " + calificacion);
				
				int resultadoMayor = calificacion.compareTo(new BigDecimal("10"));
				int resultadoMenor = calificacion.compareTo(new BigDecimal("0"));
				
				if(resultadoMayor == 1) {
					errorCalificacion.setValue("Debe ingresar una calificación entre 0 y 10");
				}
				
				if(resultadoMenor == -1) {
					errorCalificacion.setValue("Debe ingresar una calificación entre 0 y 10");
				}
			}
			
			if(errorMateria.getValue().equals("") || errorCalificacion.getValue().equals("")) {
				String idAlumno = (String) session.getAttribute("idAlumno");
				calificacionDTO.setId_t_materias(idMateria);
				calificacionDTO.setId_t_usuarios(Integer.valueOf(idAlumno));
				calificacionDTO.setCalificacion(calificacion);
				
				Response response = servicioAltaCalificacion(calificacionDTO);
				
				msg.setValue(response.getMsg());
				
				if(response.getSucces().equals("ok")) {
					msg.setStyle("color: green");
				}else {
					msg.setStyle("color: red");
				}
				
			}else {
				return;
			}

		} catch (NumberFormatException  e) {
			errorCalificacion.setValue("Debe ingresar un valor numérico");
		}
		
	}

	public void onRegresar(ForwardEvent event) {
		Executions.getCurrent().sendRedirect("calificaciones.zul");
	}
	
	private List<MateriasDTO> servicioConsultaMaterias(){
		
		String urlServicio = "http://localhost:8080/CursosAlternativosBackEnd/rest/consultaMaterias";
		String jsonResponse = ClienteREST.get(urlServicio, "application/json", "application/json");
		System.out.println("jsonResponse: " + jsonResponse);
		List<MateriasDTO> listaMaterias = null;
		try {
		
			ObjectMapper objectMapper = new ObjectMapper();
			listaMaterias = objectMapper.readValue(jsonResponse, objectMapper.getTypeFactory().constructCollectionType(List.class, MateriasDTO.class));
			System.out.println("listaMaterias: " + listaMaterias.toString());
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return listaMaterias;
		
	}
	
	private Response servicioAltaCalificacion(CalificacionesDTO calificacionDTO){
		
		String urlServicio = "http://localhost:8080/CursosAlternativosBackEnd/rest/altaCalificacion";
		String jsonRequest = new Gson().toJson(calificacionDTO);
		String jsonResponse = ClienteREST.post(urlServicio, "application/json", "application/json",jsonRequest);
		System.out.println("jsonResponse: " + jsonResponse);
		Response response = (Response) new Gson().fromJson(jsonResponse, Response.class);
		System.out.println("Response: " + response);
		
		return response;
		
	}
	
}

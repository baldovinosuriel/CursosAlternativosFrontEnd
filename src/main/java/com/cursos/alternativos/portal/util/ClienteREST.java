package com.cursos.alternativos.portal.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class ClienteREST {
	private static Logger log = Logger.getLogger("com.cursos.alternativos.portal.util.ClienteREST");
	
	/**
	 * Envía una petición POST
	 * 
	 */
	public static String post(String url,String contentType,String acceptType,String content){
		String response = "";
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost postRequest = new HttpPost(url);
		postRequest.addHeader("Content-Type", acceptType);
		postRequest.addHeader("Accept", acceptType);
		
		try{
			StringEntity input = new StringEntity(content);
	        input.setContentType(contentType);
	        postRequest.setEntity(input);
	        
	        log.info("Se envía: " + content + "\nSe espera: " + acceptType);
	        
	        HttpResponse httpResponse = httpClient.execute(postRequest);
	        
	        response = EntityUtils.toString(httpResponse.getEntity());
		}catch(Exception e){
			log.log(Level.SEVERE, "Error al realizar una peticion post a " + url,e);
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		
		return response;
	}
	
	/**
	 * Envía una petición GET
	 *
	 */
	public static String get(String url,String contentType,String acceptType){
		String response = "";
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet(url);
		getRequest.addHeader("accept", acceptType);
		getRequest.addHeader("Accept", acceptType);
		
		try{
	        
	        HttpResponse httpResponse = httpClient.execute(getRequest);
	        
	        response = EntityUtils.toString(httpResponse.getEntity());
		}catch(Exception e){
			log.log(Level.SEVERE, "Error al realizar una peticion get a " + url,e);
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		
		return response;
	}
	
	/**
	 * Envía una petición PUT
	 * 
	 */
	public static String put(String url,String contentType,String acceptType,String content){
		String response = "";
		HttpClient httpClient = new DefaultHttpClient();
		HttpPut putRequest = new HttpPut(url);
		putRequest.addHeader("Content-Type", acceptType);
		putRequest.addHeader("Accept", acceptType);
		
		try{
			StringEntity input = new StringEntity(content);
	        input.setContentType(contentType);
	        putRequest.setEntity(input);
	        
	        log.info("Se envía: " + content + "\nSe espera: " + acceptType);
	        
	        HttpResponse httpResponse = httpClient.execute(putRequest);
	        
	        response = EntityUtils.toString(httpResponse.getEntity());
		}catch(Exception e){
			log.log(Level.SEVERE, "Error al realizar una peticion put a " + url,e);
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		
		return response;
	}
	
	/**
	 * Envía una petición DELETE
	 * 
	 */
	public static String delete(String url,String contentType,String acceptType){
		String response = "";
		HttpClient httpClient = new DefaultHttpClient();
		HttpDelete deleteRequest = new HttpDelete(url);
		deleteRequest.addHeader("accept", acceptType);
		deleteRequest.addHeader("Accept", acceptType);
		
		try{
	        
	        HttpResponse httpResponse = httpClient.execute(deleteRequest);
	        
	        response = EntityUtils.toString(httpResponse.getEntity());
		}catch(Exception e){
			log.log(Level.SEVERE, "Error al realizar una peticion delete a " + url,e);
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
		
		return response;
	}
}

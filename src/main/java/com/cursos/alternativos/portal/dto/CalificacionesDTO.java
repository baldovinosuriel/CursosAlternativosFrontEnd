package com.cursos.alternativos.portal.dto;

import java.math.BigDecimal;
import java.util.Date;

public class CalificacionesDTO {
	
	private int id_t_calificaciones;
	private int id_t_materias;
	private int id_t_usuarios;
	private BigDecimal calificacion;
	private Date fecha_registro;
	
	public int getId_t_calificaciones() {
		return id_t_calificaciones;
	}
	public void setId_t_calificaciones(int id_t_calificaciones) {
		this.id_t_calificaciones = id_t_calificaciones;
	}
	public int getId_t_materias() {
		return id_t_materias;
	}
	public void setId_t_materias(int id_t_materias) {
		this.id_t_materias = id_t_materias;
	}
	public int getId_t_usuarios() {
		return id_t_usuarios;
	}
	public void setId_t_usuarios(int id_t_usuarios) {
		this.id_t_usuarios = id_t_usuarios;
	}
	public BigDecimal getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(BigDecimal calificacion) {
		this.calificacion = calificacion;
	}
	public Date getFecha_registro() {
		return fecha_registro;
	}
	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}
	@Override
	public String toString() {
		return "CalificacionesDTO [id_t_calificaciones=" + id_t_calificaciones + ", id_t_materias=" + id_t_materias
				+ ", id_t_usuarios=" + id_t_usuarios + ", calificacion=" + calificacion + ", fecha_registro="
				+ fecha_registro + "]";
	}
	
	

}

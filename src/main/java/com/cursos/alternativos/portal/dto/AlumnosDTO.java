package com.cursos.alternativos.portal.dto;

public class AlumnosDTO {
	private int id_t_usuarios;
	private String nombre;
	private String ap_paterno;
	private String ap_materno;
	private int activo;
	
	public int getId_t_usuarios() {
		return id_t_usuarios;
	}
	public void setId_t_usuarios(int id_t_usuarios) {
		this.id_t_usuarios = id_t_usuarios;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAp_paterno() {
		return ap_paterno;
	}
	public void setAp_paterno(String ap_paterno) {
		this.ap_paterno = ap_paterno;
	}
	public String getAp_materno() {
		return ap_materno;
	}
	public void setAp_materno(String ap_materno) {
		this.ap_materno = ap_materno;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	
	@Override
	public String toString() {
		return "AlumnosDTO [id_t_usuarios=" + id_t_usuarios + ", nombre=" + nombre + ", ap_paterno=" + ap_paterno
				+ ", ap_materno=" + ap_materno + ", activo=" + activo + "]";
	}
	
}
